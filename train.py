#!/usr/bin/env python3
import argparse
import colored_traceback.auto
from pathlib import Path
from typing import Optional

import lightning.pytorch as pl
from lightning.pytorch.loggers import WandbLogger
from lightning.pytorch.callbacks import RichProgressBar, ModelCheckpoint, LearningRateMonitor, LearningRateFinder
import optuna
import torch
from rich import print

import edenn
from edenn.utils import Step, FineTuneLearningRateFinder

# ==================================================================================================
class Objective:
	# ----------------------------------------------------------------------------------------------
	def __init__(self, args: argparse.Namespace):
		self.args = args

	# ----------------------------------------------------------------------------------------------
	def __call__(self, trial: Optional[optuna.trial.Trial] = None):
		self.init_trial(trial)
		self.load_checkpoint()
		self.init_model()
		self.init_trainer()

		self.trainer.fit(self.model)

	# ----------------------------------------------------------------------------------------------
	def init_trial(self, trial: Optional[optuna.trial.Trial]) -> None:
		self.trial = trial
		if self.trial is not None:
			self.args.lr = self.trial.suggest_float("lr", 1e-5, 1e-1, log=True)

	# ----------------------------------------------------------------------------------------------
	def load_checkpoint(self) -> None:
		self.checkpoint_path = None
		if self.checkpoint_path is not None:
			checkpoint = torch.load(self.checkpoint_path)
			self.args = edenn.utils.merge_args(checkpoint["hyper_parameters"]["args"], self.args)

	# ----------------------------------------------------------------------------------------------
	def init_model(self) -> None:
		print("Initialising model... ", end="")
		Model = getattr(edenn.models, self.args.model)
		self.model = Model.load_from_checkpoint(self.checkpoint_path) if self.checkpoint_path is not None else Model(self.args, self.trial)
		torch.compile(self.model)
		print("Done")

	# ----------------------------------------------------------------------------------------------
	def init_callbacks(self, logger: WandbLogger | bool, monitor: str) -> None:
		self.callbacks = []
		self.callbacks.append(RichProgressBar())
		if not self.args.nolog:
			self.callbacks.append(ModelCheckpoint(
				monitor=monitor,
				mode="min",
				save_last=True,
				save_top_k=3,
				filename="e={epoch} l={monitor:.2f}" if not self.args.overfit else "e={epoch} l={monitor:.2f}",
				auto_insert_metric_name=False,
				dirpath=Path(logger.experiment.dir) / "checkpoints" if logger else None, # type: ignore
				verbose=True,
			))
			self.callbacks.append(LearningRateMonitor(logging_interval=None, log_momentum=True))

	# ----------------------------------------------------------------------------------------------
	def init_trainer(self) -> None:
		logger = WandbLogger(project="EDeNN",
				name=self.args.name,
				config=self.args,
				save_code=True,
				log_model=True, # If "all", then `save_top_k` is not really respected in the cloud
			) if not self.args.nolog else False
		if logger:
			logger.experiment.log_code(Path(__file__).parent.resolve())
		monitor = f"loss/{Step.VAL}" if not self.args.overfit else f"loss/{Step.TRAIN}"
		self.init_callbacks(logger, monitor)
		self.trainer = pl.Trainer(
			logger=logger,
			max_epochs=self.args.max_epochs,
			callbacks=self.callbacks,
			limit_train_batches=self.args.limit_train,
			limit_val_batches=self.args.limit_val,
			overfit_batches=self.args.overfit,
		)
		if logger:
			logger.experiment.watch(self.model, log="all") # Log gradients


# ==================================================================================================
def init_script():
	torch.set_printoptions(precision=16, sci_mode=False)
	torch.set_float32_matmul_precision("medium") # or "high"


# ==================================================================================================
if __name__ == "__main__":
	init_script()
	args = edenn.utils.parse_args()
	if args.seed is not None:
		pl.seed_everything(seed=args.seed, workers=True)
	if args.optuna is not None:
		study = optuna.create_study(
			study_name=f"{args.model}_{args.dataset}",
			direction=optuna.study.StudyDirection.MINIMIZE,
			storage=args.optuna,
			load_if_exists=True
		)
		optuna.create_study()
		study.optimize(Objective(args), n_trials=100, n_jobs=1, gc_after_trial=False)
		print(f"Best params so far: {study.best_params}")
	else:
		Objective(args)()
