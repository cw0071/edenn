import setuptools

setuptools.setup(
	name="EDeNN",
	version="0.1",
	description="Event Decay Neural Networks",
	author="Celyn Walters",
	author_email="celyn.walters@surrey.ac.uk",
	url="https://gitlab.surrey.ac.uk/cw0071/edenn",
	packages=setuptools.find_packages(),
	classifiers=[
		"Programming Language :: Python :: 3",
		"Operating System :: OS Independent",
	],
	python_requires=">=3.10",
)
