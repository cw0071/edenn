import argparse
import inspect
import textwrap
from typing import Optional

import edenn

# ==================================================================================================
class ArgumentParser(argparse.ArgumentParser):
	"""Override argparse.ArgumentParser."""
	# ----------------------------------------------------------------------------------------------
	def error(self, message: str):
		"""
		Print the problems with the arguments.
		Also prints a helpful message about model types.

		Args:
			message (str): Message to print
		"""
		# self.print_usage(sys.stderr)
		self.print_help() # Also prints usage
		args = {"prog": self.prog, "message": message}
		self.exit(2, textwrap.dedent((f"""\
			{args['prog']}: error: {args['message']}
			More help options will be provided when specifying the model and dataset types.
		""")))


# ==================================================================================================
def limit_float_int(limit: Optional[str]) -> Optional[float | int]:
	if limit is None:
		return limit

	return float(limit) if "." in str(limit) else int(limit)


# ==================================================================================================
def parse_args() -> argparse.Namespace:
	# Help argument must only be added once, so we first go through as many arg groups as we can
	parser = ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, add_help=False)

	models = [name for name, obj in inspect.getmembers(edenn.models) if inspect.isclass(obj) and name != "Base"]
	datasets = [name for name, obj in inspect.getmembers(edenn.datasets) if inspect.isclass(obj) and name != "Base"]

	group = parser.add_argument_group("Trainer")
	group.add_argument("--optuna", type=str, help="Optimise with optuna using this storage URL. Examples: 'sqlite:///optuna.db' or 'postgresql://postgres:password@host:5432/postgres'")
	group.add_argument("--seed", type=int, help="Use specified random seed for everything", default=None)
	group.add_argument("--lr", type=float, default=0.01, help="Learning rate")
	group.add_argument("--nolog", action="store_true", help="Don't log to wandb")
	group.add_argument("--limit_train", type=str, help="Use this train set proportion (float) or batches (int) each epoch (still randomised over entire dataset)", default=None)
	group.add_argument("--limit_val", type=str, help="Use this val set proportion (float) or batches (int) each epoch (still randomised over entire dataset)", default=None)
	group.add_argument("--overfit", type=str, help="Overfit to this proportion (float) or batches (int), use train set for val", default="0.0")
	group.add_argument("--max_epochs", type=int, help="Maximum number of epochs", default=-1)

	parser.add_argument("name", type=str, help="Name of run")
	parser.add_argument("model", choices=models, metavar=f"MODEL: {{{', '.join(models)}}}", help="Model to train")
	parser.add_argument("dataset", choices=datasets, metavar=f"DATASET: {{{', '.join(datasets)}}}", help="Dataset to train/val/test on")
	args_known, _ = parser.parse_known_args()
	if args_known.model is None or args_known.dataset is None:
		parser.add_argument("-h", "--help", action="help", default=argparse.SUPPRESS, help="Show this help message and exit.")
	Model = getattr(edenn.models, args_known.model)
	parser = Model.add_argparse_args(parser) # Will chain to all child parser groups
	Dataset = getattr(edenn.datasets, args_known.dataset)
	parser = Dataset.add_argparse_args(parser) # Will chain to all child parser groups

	try: # If we didn't add the help already (because an essential option was missing), add it now
		parser.add_argument("-h", "--help", action="help", default=argparse.SUPPRESS, help="Show this help message and exit.")
	except argparse.ArgumentError:
		pass

	args = parser.parse_args()
	args.limit_train = limit_float_int(args.limit_train)
	args.limit_val = limit_float_int(args.limit_val)
	args.overfit = limit_float_int(args.overfit)

	return args


# ==================================================================================================
def merge_two_dicts(original: dict, updating: dict) -> dict:
	"""Adds dictionary entries from `updating` to `original`, overwriting if necessary."""
	new_dict: dict = original.copy() # Start with keys and values of starting_dict
	new_dict.update(updating) # Modifies starting_dict with keys and values of updater_dict

	return new_dict


# ==================================================================================================
# https://stackoverflow.com/questions/56136549/how-can-i-merge-two-argparse-namespaces-in-python-2-x
def merge_args(original: argparse.Namespace | dict, updating: argparse.Namespace) -> argparse.Namespace:
	"""Adds args from `updating` to `original`, overwriting if necessary."""
	# vars() returns the __dict__ attribute for values of the given object, e.g. {field:value}
	if isinstance(original, dict):
		merged_key_values_for_namespace: dict = merge_two_dicts(original, vars(updating))
	elif isinstance(original, argparse.Namespace):
		merged_key_values_for_namespace: dict = merge_two_dicts(vars(original), vars(updating))

	return argparse.Namespace(**merged_key_values_for_namespace)
