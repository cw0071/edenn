import argparse
from pathlib import Path

import torch
import numpy as np
import h5py

from edenn.datasets import Base
from edenn.utils import Step

# ==================================================================================================
# Dataset from https://github.com/uzh-rpg/snn_angular_velocity
class AngularVelocity(Base):
	height = 180
	width = 240
	input_shape = 2 # P, N
	output_shape = 3 # X, Y, Z
	# ----------------------------------------------------------------------------------------------
	def __init__(self, args: argparse.Namespace, step: Step) -> None:
		super().__init__(args=args, step=step)
		parent = args.dataset_path / str(step)
		self.samples = ([p for p in parent.rglob("*.h5")])

	# ----------------------------------------------------------------------------------------------
	def __len__(self):
		return len(self.samples)

	# ----------------------------------------------------------------------------------------------
	def __getitem__(self, index: int):
		subseq_file = self.samples[index]

		data = dict()
		if Path(subseq_file).suffix == ".npz":
			np_data = np.load(subseq_file)

			data["ev_xy"] = torch.from_numpy(np_data["ev_xy"])
			data["ev_pol"] = torch.from_numpy(np_data["ev_pol"])
			data["ev_ts_us"] = torch.from_numpy(np_data["ev_ts"])
			data["ang_xyz"] = torch.from_numpy(np_data["ang_xyz"])
			data["ang_ts_us"] = torch.from_numpy(np_data["ang_ts"])
		else:
			assert Path(subseq_file).suffix == ".h5"
			with h5py.File(subseq_file, "r") as hf:
				data["ev_xy"] = torch.from_numpy(hf["ev_xy"][()])
				data["ev_ts_us"] = torch.from_numpy(hf["ev_ts"][()])
				data["ev_pol"] = torch.from_numpy(hf["ev_pol"][()])
				data["ang_xyz"] = torch.from_numpy(hf["ang_xyz"][()])
				data["ang_ts_us"] = torch.from_numpy(hf["ang_ts"][()])

		n_bins = data["ang_ts_us"].numel()
		step_size = n_bins / self.args.n_bins
		filtered_indices = [round(i * step_size) for i in range(self.args.n_bins)]
		data["ang_xyz"] = torch.index_select(data["ang_xyz"], 0, torch.tensor(filtered_indices))
		assert data["ang_xyz"].numel() == self.args.n_bins * 3

		assert self.generator
		spike_tensor = self.generator.getSlayerSpikeTensor(
			data["ev_pol"],
			data["ev_xy"],
			data["ev_ts_us"],
		)
		data["ang_xyz"] = data["ang_xyz"].t()

		assert data["ang_xyz"].size(0) == 3
		out = {
			"file_number": int("".join(filter(str.isdigit, Path(subseq_file).stem))),
			"spike_tensor": spike_tensor,
			"angular_velocity": data["ang_xyz"].float(),
		}
		assert len(data) == 5

		return out
