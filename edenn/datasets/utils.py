import torch

# ==================================================================================================
# Adapted from https://github.com/uzh-rpg/snn_angular_velocity
class SpikeRepresentationGenerator:
	# ----------------------------------------------------------------------------------------------
	def __init__(self, width: int, height: int, num_time_bins: int):
		"""
		Class to generate spikes from event tensors.

		Args:
			width (int): Width of event image
			height (int): Height of event image
			num_time_bins (int): Number of time bins within the window
		"""
		self.width = width
		self.height = height
		self.num_time_bins = num_time_bins

	# ----------------------------------------------------------------------------------------------
	def getSlayerSpikeTensor(self, polarities: torch.Tensor, locations: torch.Tensor,
			timestamps: torch.Tensor) -> torch.Tensor:
		"""
		Generate spikes from event tensors.
		All arguments must be of the same image shape.

		Args:
			polarities (torch.Tensor): Event polarities
			locations (torch.Tensor): Event XY coordinates
			timestamps (torch.Tensor): Event timestamps in microseconds

		Returns:
			torch.Tensor: Spike train tensor
		"""
		spike_tensor = torch.zeros((2, self.height, self.width, self.num_time_bins))
		if len(timestamps) < 2:
			return spike_tensor # Empty tensor, don't raise an error

		binDuration = (timestamps[-1] - timestamps[0]) / self.num_time_bins
		if binDuration == 0:
			return spike_tensor
		time_idx = ((timestamps - timestamps[0]) / binDuration) # Assumed that events are sorted ascending!
		# Handle time stamps that are not floored and would exceed the allowed index after to-index conversion
		time_idx[time_idx >= self.num_time_bins] = self.num_time_bins - 1

		spike_tensor[polarities.long(), locations[:, 1].long(), locations[:, 0].long(), time_idx.long()] = 1

		return spike_tensor
