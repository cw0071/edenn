# Evaluation from https://github.com/uzh-rpg/snn_angular_velocity
from pathlib import Path

import torch
import numpy as np

# ==================================================================================================
def medianRelativeError(pred: np.ndarray, gt: np.ndarray):
	# pred/gt: [S, 3, D)
	gt_norm = np.linalg.norm(gt, ord=2, axis=1).flatten()
	diff_norm = np.linalg.norm(gt - pred, ord=2, axis=1).flatten()

	# Do not take ground truth into account that is less than 1 deg/s.
	one_deg_ps = 1 / 180 * np.pi
	is_valid = (np.abs(gt_norm) >= one_deg_ps)
	diff_norm = diff_norm[is_valid]
	gt_norm = gt_norm[is_valid]
	rel_errors = np.divide(diff_norm, gt_norm)

	return np.median(rel_errors)


# ==================================================================================================
def rmse(pred: np.ndarray, gt: np.ndarray, deg: bool=True):
	# pred/gt: [S, 3, D)
	rmse = np.mean(np.sqrt(np.sum(np.square(pred - gt), axis=1).flatten()))
	if deg:
		rmse = rmse / np.pi * 180

	return rmse


# ==================================================================================================
class DataCollector:
	# ----------------------------------------------------------------------------------------------
	def __init__(self, loss_start_idx: int):
		assert loss_start_idx >= 0
		self.loss_start_idx = loss_start_idx
		self.data_gt = []
		self.data_pred = []

	# ----------------------------------------------------------------------------------------------
	def append(self, pred: torch.Tensor, gt: torch.Tensor):
		self.data_pred.append(pred.detach().cpu().numpy()) # [B, 3, D]
		self.data_gt.append(gt.detach().cpu().numpy()) # [B, 3, D]

	# ----------------------------------------------------------------------------------------------
	def writeToDisk(self, out_dir: Path):
		pred = np.concatenate(self.data_pred, axis=0)
		gt = np.concatenate(self.data_gt, axis=0)
		np.save(out_dir / "predictions.npy", pred)
		np.save(out_dir / "groundtruth.npy", gt)

	# ----------------------------------------------------------------------------------------------
	def calculate_errors(self):
		pred = np.concatenate(self.data_pred, axis=0) # [2495, 3, D]
		gt = np.concatenate(self.data_gt, axis=0) # [2495, 3, D]
		pred = pred[..., self.loss_start_idx:] # [2495, 3, d]
		gt = gt[..., self.loss_start_idx:] # [2495, 3, d]

		self.relative_error = medianRelativeError(pred, gt)
		self.rmse = rmse(pred, gt, deg=True)
