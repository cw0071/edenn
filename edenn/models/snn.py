#!/usr/bin/env python3
import dataclasses

import torch
# from slayerSNN.slayer import spikeLayer # New
import slayerpytorch as snn # Old
from rich import print, inspect

from edenn.datasets import AngularVelocity as Dataset
from edenn.utils import Step
from edenn.models import Base
from edenn.models.utils import MetaTensor, TensorLayout, DataType, NeuronConfig

# ==================================================================================================
class SNN(Base):
	_input_key: str = "input"
	_output_key: str = "output"
	# ----------------------------------------------------------------------------------------------
	def __init__(self, args, trial=None):
		super().__init__(args, trial)
		self.dataset_class = Dataset
		self.simulation_params = {
			"Ts": (self.hparams.window_length / self.hparams.n_bins) * 1e3, # Length of time for each bin (ms)
			"tSample": self.hparams.window_length * 1e3, # Length of time for the window (ms)
		}
		self.init_layers()
		self._data = dict()

	# ----------------------------------------------------------------------------------------------
	def init_layers(self):
		neuron_config_conv1 = dataclasses.asdict(NeuronConfig(
			theta=0.27,
			tauSr=2.0,
			tauRef=1.0,
			scaleRho=0.20,
		))
		neuron_config_conv2 = dataclasses.asdict(NeuronConfig(
			theta=0.25,
			tauSr=2.0,
			tauRef=1.0,
			scaleRho=0.13,
		))
		neuron_config_conv3 = dataclasses.asdict(NeuronConfig(
			theta=0.3,
			tauSr=4.0,
			tauRef=4.0,
			scaleRho=0.13,
		))
		neuron_config_conv4 = dataclasses.asdict(NeuronConfig(
			theta=0.4,
			tauSr=4.0,
			tauRef=4.0,
			scaleRho=0.25,
		))
		neuron_config_conv5 = dataclasses.asdict(NeuronConfig(
			theta=0.4,
			tauSr=4.0,
			tauRef=4.0,
			scaleRho=100.0,
		))
		# NOTE: tauSr is the only parameter that has any influence on the output or the gradients
		neuron_config_fc = dataclasses.asdict(NeuronConfig(tauSr=8.0))

		# self.slayer_conv1 = spikeLayer(neuron_config_conv1, self.simulation_params)
		# self.slayer_conv2 = spikeLayer(neuron_config_conv2, self.simulation_params)
		# self.slayer_conv3 = spikeLayer(neuron_config_conv3, self.simulation_params)
		# self.slayer_conv4 = spikeLayer(neuron_config_conv4, self.simulation_params)
		# self.slayer_conv5 = spikeLayer(neuron_config_conv5, self.simulation_params)
		# self.slayer_fc = spikeLayer(neuron_config_fc, self.simulation_params)
		self.slayer_conv1 = snn.layer(neuron_config_conv1, self.simulation_params)
		self.slayer_conv2 = snn.layer(neuron_config_conv2, self.simulation_params)
		self.slayer_conv3 = snn.layer(neuron_config_conv3, self.simulation_params)
		self.slayer_conv4 = snn.layer(neuron_config_conv4, self.simulation_params)
		self.slayer_conv5 = snn.layer(neuron_config_conv5, self.simulation_params)
		self.slayer_fc = snn.layer(neuron_config_fc, self.simulation_params)

		conv1_out_channels = 16
		conv2_out_channels = 32
		conv3_out_channels = 64
		conv4_out_channels = 128
		conv5_out_channels = 256

		self.conv1 = self.slayer_conv1.conv(
			inChannels=self.dataset_class.input_shape, outChannels=conv1_out_channels,
			kernelSize=3, stride=2, padding=0, dilation=1, groups=1, weightScale=1,
		)
		self.conv2 = self.slayer_conv2.conv(
			inChannels=conv1_out_channels, outChannels=conv2_out_channels,
			kernelSize=3, stride=2, padding=0, dilation=1, groups=1, weightScale=1,
		)
		self.conv3 = self.slayer_conv3.conv(
			inChannels=conv2_out_channels, outChannels=conv3_out_channels,
			kernelSize=3, stride=2, padding=0, dilation=1, groups=1, weightScale=1,
		)
		self.conv4 = self.slayer_conv3.conv(
			inChannels=conv3_out_channels, outChannels=conv4_out_channels,
			kernelSize=3, stride=2, padding=0, dilation=1, groups=1, weightScale=1,
		)
		self.conv5 = self.slayer_conv3.conv(
			inChannels=conv4_out_channels, outChannels=conv5_out_channels,
			kernelSize=3, stride=1, padding=0, dilation=1, groups=1, weightScale=1,
		)
		self.fc = self.slayer_fc.dense(inFeatures=conv5_out_channels, outFeatures=self.dataset_class.output_shape, weightScale=1)

	# ----------------------------------------------------------------------------------------------
	def step(self, batch, batch_idx, step: Step):
		spikes = batch["spike_tensor"] # [B, C=2, H, W, D]
		gt = batch["angular_velocity"] # [B, C=3, D]
		spikes = spikes.permute(0, 1, 4, 2, 3) # NCHWD -> NCDHW

		pred = self(spikes) # [B, C=3, 100]

		pred = pred[..., self.hparams.loss_ignore_t:]
		gt = gt[..., self.hparams.loss_ignore_t:]
		loss = self.criterion(pred, gt)
		loss = loss.mean()

		# Don't log anything, especially important to stop optuna pruning here
		if self.trainer.sanity_checking:
			return loss

		pred = pred.detach().cpu()
		gt = gt.detach().cpu()

		self.log(f"loss/{step}", loss, on_epoch=True, on_step=False)
		if self.logger:
			if batch_idx == 0:
				spikes = spikes.sum(dim=2) # Over D
				if self.decide_log_media(step):
					self.logger.log_image(key=f"input/{step}", images=[*torch.cat((spikes, torch.zeros_like(spikes[:, :1, ...])), dim=1)])

			if self.hparams.overfit > 0 and batch_idx == 0:
				spikes = spikes.sum(dim=-3) # Over D
				self.log(f"{step}/gt_ax", gt[0, 0, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/gt_ay", gt[0, 1, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/gt_az", gt[0, 2, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/pred_ax", pred[0, 0, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/pred_ay", pred[0, 1, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/pred_az", pred[0, 2, 0], on_epoch=True, on_step=False)

		return loss

	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor) -> torch.Tensor:
		# NOTE: spikeLayer.conv expects tensors in NCHWT. Need to permute from standard BCDHW
		x = x.permute(0, 1, 3, 4, 2) # BCDHW -> NCHWT

		self.addInputMetaTensor(MetaTensor(x, TensorLayout.Conv, DataType.Spike))

		spikes_layer_1 = self.slayer_conv1.spike(self.conv1(self.slayer_conv1.psp(x)))
		spikes_layer_2 = self.slayer_conv2.spike(self.conv2(self.slayer_conv2.psp(spikes_layer_1)))
		spikes_layer_3 = self.slayer_conv3.spike(self.conv3(self.slayer_conv3.psp(spikes_layer_2)))
		spikes_layer_4 = self.slayer_conv4.spike(self.conv4(self.slayer_conv4.psp(spikes_layer_3)))
		spikes_layer_5 = self.slayer_conv5.spike(self.conv5(self.slayer_conv5.psp(spikes_layer_4)))

		self.addMetaTensor("conv1", MetaTensor(spikes_layer_1, TensorLayout.Conv, DataType.Spike))
		self.addMetaTensor("conv2", MetaTensor(spikes_layer_2, TensorLayout.Conv, DataType.Spike))
		self.addMetaTensor("conv3", MetaTensor(spikes_layer_3, TensorLayout.Conv, DataType.Spike))
		self.addMetaTensor("conv4", MetaTensor(spikes_layer_4, TensorLayout.Conv, DataType.Spike))
		self.addMetaTensor("conv5", MetaTensor(spikes_layer_5, TensorLayout.Conv, DataType.Spike))

		# Apply average pooling on spike-trains.
		spikes_mean = torch.mean(spikes_layer_5, dim=(2, 3), keepdims=True) # type: ignore
		psp_out = self.slayer_fc.psp(self.fc(spikes_mean))
		self.addOutputMetaTensor(MetaTensor(psp_out, TensorLayout.FC, DataType.Dense))

		assert psp_out.shape[1] == self.dataset_class.output_shape

		psp_out = psp_out.permute(0, 1, 4, 2, 3) # NCHWT -> BCDHW

		return psp_out.squeeze(-1).squeeze(-1) # [1, self.dataset_class.output_shape]

	# ----------------------------------------------------------------------------------------------
	def addMetaTensor(self, key: str, value: MetaTensor):
		assert not key == self._output_key, "Use addOutputMetaTensor function instead"
		self._data[key] = value

	# ----------------------------------------------------------------------------------------------
	def addInputMetaTensor(self, value: MetaTensor):
		assert value.isSpikeType()
		assert value.hasConvLayout(), "Does not have to be but is reasonable for the moment"
		self._data[self._input_key] = value

	# ----------------------------------------------------------------------------------------------
	def addOutputMetaTensor(self, value: MetaTensor):
		assert value.isDenseType()
		assert value.hasFCLayout(), "Does not have to be but is reasonable for the moment"
		self._data[self._output_key] = value

	# ----------------------------------------------------------------------------------------------
	def test_step(self, batch, batch_idx):
		spikes = batch["spike_tensor"] # [B, C=2, H, W, D]
		gt = batch["angular_velocity"] # [B, C=3, D]
		spikes = spikes.permute(0, 1, 4, 2, 3) # NCHWD -> NCDHW

		pred = self(spikes) # [B, C=3, 100]

		self.data_collector.append(pred, gt)

		loss = self.criterion(pred[..., :-1], gt[..., :-1])
		loss = loss.mean()

		self.log(f"loss/{Step.TEST}", loss, on_epoch=True, on_step=False)

		return loss


# ==================================================================================================
if __name__ == "__main__":
	import argparse
	from rich import print

	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, allow_abbrev=False)
	parser = SNN.add_argparse_args(parser)
	args = parser.parse_args()

	model = SNN(args=args)

	width = 346
	height = 260

	event_tensor = torch.rand([1, 2, model.hparams.n_bins, height, width])
	print(f"input event shape: {event_tensor.shape}")
	print(f"target shape: {model.dataset_class.output_shape}")

	model = model.to("cuda")
	event_tensor = event_tensor.to("cuda")
	output = model(event_tensor)

	print(f"model: {model}")
	print(f"layer 1: {model._data['conv1'].getTensor().shape}")
	print(f"layer 2: {model._data['conv2'].getTensor().shape}")
	print(f"layer 3: {model._data['conv3'].getTensor().shape}")
	print(f"layer 4: {model._data['conv4'].getTensor().shape}")
	print(f"layer 5: {model._data['conv5'].getTensor().shape}")
	print(f"output features shape: {output.shape}")
