import argparse
from abc import abstractmethod
from typing import Optional

import torch
from torch.utils.data import DataLoader
import lightning.pytorch as pl
import optuna
import wandb
from rich import print

from edenn.utils import Step
from edenn.models.utils import DataCollector

# ==================================================================================================
class Base(pl.LightningModule):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, args: argparse.Namespace, trial: Optional[optuna.trial.Trial] = None):
		super().__init__()
		self.save_hyperparameters(args) # Moves args to self.hparams
		self.trial = trial
		self.criterion = torch.nn.MSELoss(reduction="none")
		self.hparams.loss_ignore_t = min(self.hparams.loss_ignore_t, self.hparams.n_bins - 1)

	# ----------------------------------------------------------------------------------------------
	@staticmethod
	def add_argparse_args(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
		"""
		Add model-specific arguments.

		Args:
			parser (argparse.ArgumentParser): Main parser

		Returns:
			argparse.ArgumentParser: Modified parser
		"""
		group = parser.add_argument_group("Model")
		group.add_argument("--window_length", type=float, help="Time-discretization in seconds", default=1e-3)
		group.add_argument("--n_bins", type=int, help="Number of simulation steps", default=100)
		group.add_argument("--loss_ignore_t", type=int, help="Ignore the first N bins for computing loss", default=50)

		return parser

	# ----------------------------------------------------------------------------------------------
	@abstractmethod
	def init_layers(self):
		raise NotImplementedError

	# ----------------------------------------------------------------------------------------------
	def configure_optimizers(self):
		return torch.optim.Adam(self.parameters(), lr=self.hparams.lr, amsgrad=True)

	# ----------------------------------------------------------------------------------------------
	def dataloader(self, step: Step) -> DataLoader:
		print(f"Initialising {step} dataset... ", end="") # FIX rich print isn't respecting flush here?
		loader = DataLoader(
			self.dataset_class(self.hparams, step),
			batch_size=(1 if step == Step.TEST else self.hparams.batch_size),
			shuffle=(step == Step.TRAIN),
			num_workers=self.hparams.workers,
			pin_memory=self.device != torch.device("cpu"),
			persistent_workers=self.hparams.workers > 0,
			drop_last=(step != Step.TEST),
		)
		print("Done")

		return loader

	# ----------------------------------------------------------------------------------------------
	def train_dataloader(self) -> DataLoader:
		return self.dataloader(Step.TRAIN)

	# ----------------------------------------------------------------------------------------------
	def val_dataloader(self) -> DataLoader:
		return self.dataloader(Step.VAL)

	# ----------------------------------------------------------------------------------------------
	def test_dataloader(self) -> DataLoader:
		return self.dataloader(Step.TEST)

	# ----------------------------------------------------------------------------------------------
	def process(self, layer: torch.nn.Module, x: torch.Tensor | tuple[torch.Tensor, torch.Tensor]):
		if isinstance(layer, torch.nn.Linear):
			assert isinstance(x, torch.Tensor)
			x = x.mean(dim=(3, 4)) # BCDHW -> BCD
			x = x.permute(0, 2, 1) # BCD -> BDC
			x = layer(x)
			x = x.permute(0, 2, 1) # BDC -> BCD
		else:
			x = layer(x)

		return x

	# ----------------------------------------------------------------------------------------------
	def forward(self, x: torch.Tensor | tuple[torch.Tensor, torch.Tensor]):
		"""Forward pass."""
		for layer in self.layers:
			x = self.process(layer, x)

		return x

	# ----------------------------------------------------------------------------------------------
	@abstractmethod
	def step(self, batch, batch_idx, step: Step):
		raise NotImplementedError

	# ----------------------------------------------------------------------------------------------
	def training_step(self, batch, batch_idx):
		return self.step(batch, batch_idx, Step.TRAIN)

	# ----------------------------------------------------------------------------------------------
	def validation_step(self, batch, batch_idx):
		return self.step(batch, batch_idx, Step.VAL)

	# ----------------------------------------------------------------------------------------------
	def test_step(self, batch, batch_idx):
		return self.step(batch, batch_idx, Step.VAL)

	# ----------------------------------------------------------------------------------------------
	def decide_log_media(self, step: Step) -> bool:
		if step == Step.TRAIN:
			if isinstance(self.trainer.overfit_batches, float): # type: ignore
				return self.trainer.overfit_batches == 1.0 # type: ignore
			else:
				return self.current_epoch == 0
		else:
			return self.current_epoch == 0

	# ----------------------------------------------------------------------------------------------
	@classmethod
	def load_from_checkpoint(self, checkpoint_path: str, *args, **kwargs):
		print("Loading checkpoint...")
		return super().load_from_checkpoint(checkpoint_path, *args, **kwargs)

	# ----------------------------------------------------------------------------------------------
	def on_train_start(self) -> None:
		if self.logger:
			wandb.define_metric(f"loss/{Step.TRAIN}", summary="min")
			wandb.define_metric(f"loss/{Step.VAL}", summary="min")

	# ----------------------------------------------------------------------------------------------
	def on_test_start(self):
		self.data_collector = DataCollector(self.hparams.loss_ignore_t)
		if self.logger:
			wandb.define_metric(f"Relative error", summary="min")
			wandb.define_metric(f"RMSE", summary="min")

	# ----------------------------------------------------------------------------------------------
	def on_test_epoch_end(self):
		self.data_collector.calculate_errors()
		self.log("Relative error", self.data_collector.relative_error)
		self.log("RMSE", self.data_collector.rmse)
