#!/usr/bin/env python3
import torch
import wandb

from edenn.datasets import AngularVelocity as Dataset
from edenn.models import Base
from edenn.utils import Step
from edenn.models.utils import Decay3D

# ==================================================================================================
class AngularVelocity(Base):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, args, trial=None):
		super().__init__(args, trial)
		self.dataset_class = Dataset
		self.init_layers()

	# ----------------------------------------------------------------------------------------------
	def init_layers(self):
		conv = Decay3D
		self.conv1 = torch.nn.Sequential(
			conv(self.dataset_class.input_shape, 16, kernel_size=3, stride=2),
			# torch.nn.BatchNorm3d(num_features=16),
			torch.nn.ReLU(inplace=True),
		) # [B, C=16, D=100, H=89, W=119]
		self.conv2 = torch.nn.Sequential(
			conv(16, 32, kernel_size=3, stride=2),
			# torch.nn.BatchNorm3d(num_features=32),
			torch.nn.ReLU(inplace=True),
		) # [B, C=32, D=100, H=44, W=59]
		self.conv3 = torch.nn.Sequential(
			conv(32, 64, kernel_size=3, stride=2),
			# torch.nn.BatchNorm3d(num_features=64),
			torch.nn.ReLU(inplace=True),
		) # [B, C=64, D=100, H=21, W=29]
		self.conv4 = torch.nn.Sequential(
			conv(64, 128, kernel_size=3, stride=2),
			# torch.nn.BatchNorm3d(num_features=128),
			torch.nn.ReLU(inplace=True),
		) # [B, C=128, D=100, H=10, W=14]
		self.conv5 = torch.nn.Sequential(
			conv(128, 256, kernel_size=3, stride=1),
			# torch.nn.BatchNorm3d(num_features=256),
			torch.nn.ReLU(inplace=True),
		) # [B, C=256, D=100, H=10, W=14]
		self.fc = torch.nn.Linear(256, self.dataset_class.output_shape, bias=False) # [8, 13, 100]
		self.layers = torch.nn.Sequential(
			self.conv1,
			self.conv2,
			self.conv3,
			self.conv4,
			self.conv5,
			self.fc,
		)

	# ----------------------------------------------------------------------------------------------
	def step(self, batch, batch_idx, step: Step):
		spikes = batch["spike_tensor"] # [B, C=2, H, W, D]
		gt = batch["angular_velocity"] # [B, C=3, D]
		spikes = spikes.permute(0, 1, 4, 2, 3) # NCHWD -> NCDHW

		pred = self(spikes) # [B, C=3, 100]

		pred = pred[..., self.hparams.loss_ignore_t:]
		gt = gt[..., self.hparams.loss_ignore_t:]
		loss = self.criterion(pred, gt)
		loss = loss.mean()

		# Don't log anything, especially important to stop optuna pruning here
		if self.trainer.sanity_checking:
			return loss

		pred = pred.detach().cpu()
		gt = gt.detach().cpu()

		self.log(f"loss/{step}", loss, on_epoch=True, on_step=False)
		if self.logger:
			if self.trainer.global_step == 0:
				wandb.define_metric(f"loss/{step}", summary="min")
			if batch_idx == 0:
				spikes = spikes.sum(dim=2) # Over D
				if self.decide_log_media(step):
					self.logger.log_image(key=f"input/{step}", images=[*torch.cat((spikes, torch.zeros_like(spikes[:, :1, ...])), dim=1)])

			if self.hparams.overfit > 0 and batch_idx == 0:
				spikes = spikes.sum(dim=-3) # Over D
				self.log(f"{step}/gt_ax", gt[0, 0, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/gt_ay", gt[0, 1, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/gt_az", gt[0, 2, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/pred_ax", pred[0, 0, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/pred_ay", pred[0, 1, 0], on_epoch=True, on_step=False)
				self.log(f"{step}/pred_az", pred[0, 2, 0], on_epoch=True, on_step=False)

		return loss

	# ----------------------------------------------------------------------------------------------
	def test_step(self, batch, batch_idx):
		spikes = batch["spike_tensor"] # [B, C=2, H, W, D]
		gt = batch["angular_velocity"] # [B, C=3, D]
		spikes = spikes.permute(0, 1, 4, 2, 3) # NCHWD -> NCDHW

		pred = self(spikes) # [B, C=3, 100]

		self.data_collector.append(pred, gt)

		loss = self.criterion(pred[..., :-1], gt[..., :-1])
		loss = loss.mean()

		self.log(f"loss/{Step.TEST}", loss, on_epoch=True, on_step=False)


# ==================================================================================================
if __name__ == "__main__":
	import argparse
	from rich import print

	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, allow_abbrev=False)
	parser = AngularVelocity.add_argparse_args(parser)
	args = parser.parse_args()

	model = AngularVelocity(args=args)

	width = 346
	height = 260

	event_tensor = torch.rand([1, 2, model.hparams.n_bins, height, width])
	print(f"input event shape: {event_tensor.shape}")
	print(f"target shape: {model.dataset_class.output_shape}")

	model = model.to("cuda")
	event_tensor = event_tensor.to("cuda")
	output = model(event_tensor)

	print(f"model: {model}")
	print(f"output features shape: {output.shape}")
